package com.example.university;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    Button btnScheduleActivity;
    private static final String TAG = "main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "MainActivity создано.");
        setContentView(R.layout.activity_main);

        btnScheduleActivity = findViewById(R.id.btnScheduleActivity);
        btnScheduleActivity.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnScheduleActivity:
                Intent intent1 = new Intent(this, Main2Activity.class);
                startActivity(intent1);
                break;
            default:
                break;
        }
    }
}
