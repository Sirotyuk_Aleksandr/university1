package com.example.university;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class Main2Activity extends AppCompatActivity implements OnClickListener{
    Button btnFirstWeek;
    Button btnSecondWeek;
    private static final String TAG = "main2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnFirstWeek = findViewById(R.id.btnFirstWeek);
        btnFirstWeek.setOnClickListener(this);

        btnSecondWeek = findViewById(R.id.btnSecondWeek);
        btnSecondWeek.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFirstWeek:
                Intent intent1 = new Intent(this, ScheduleActivity.class);
                startActivity(intent1);
                break;
            case R.id.btnSecondWeek:
                Intent intent2 = new Intent(this, Schedule2Activity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }
}
